import React, {forwardRef, memo} from 'react';
import {Text, View, Dimensions} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import {SeatItem} from './componentBuilder';

import {
  appDimensions,
  Colors,
  FONT_BOLD,
  FONT_REGULAR,
} from './../services/styles';
import ZoomHandler from '../zoom_view/ZoomHandler';
interface SeatLayoutInterface{
  seatRows : any,
  isSeatClicable : any,
  isReservable : any,
  isSellable : any ,
  onSeatSelected : any,
  loading : boolean,
  styles : any,
  ref : any
}
const {width, height} = Dimensions.get('window');

const SeatLayoutC: React.FC<SeatLayoutInterface> = forwardRef((props, ref) => {
  const {
    seatRows,
    isSeatClicable,
    isReservable,
    isSellable,
    onSeatSelected,
    loading,
    styles,
  } = props;

  return (
    <ZoomHandler
      containerSize={{
        height: width + 40,
        width: width,
      }}
      ref={ref}
      minZoom={1}
      maxZoom={3}
      zoomLevels={2}>
      <View
        style={{
          // flex: 1,
          // justifyContent: "center",
          alignItems: 'center',
          width: width,
        }}>
        {seatRows.map(row => {
          return (
            <View key={Math.random()} style={{flexDirection: 'row'}}>
              {row.map(seat => {
                const isSeatEnabled =
                  isSeatClicable(seat.ShowSeatStatus) &&
                  (isReservable || isSellable);
                const seatButtonWidth = width / row.length - moderateScale(3.5);
                const seatButtonHeight =
                  width / seatRows.length - moderateScale(3.5);
                const actualSize =
                  seatButtonHeight > seatButtonHeight
                    ? seatButtonHeight
                    : seatButtonWidth;
                const isSeatLabelNumber = Number(seat.SeatLabel);
                console.log(isSeatEnabled, 'endbled');
                return (
                  <SeatItem
                    key={Math.random()}
                    disable={!isSeatEnabled}
                    seat={seat}
                    onClick={() => onSeatSelected(seat)}
                    buttonStyle={{
                      width: moderateScale(actualSize),
                      height: moderateScale(actualSize),
                    }}
                    buttonTextStyle={{
                      fontSize: isSeatLabelNumber
                        ? moderateScale(seatButtonWidth * 0.6)
                        : moderateScale(seatButtonWidth * 0.6),
                      color: isSeatLabelNumber ? Colors.WHITE : Colors.BLACK,
                      // fontFamily: isSeatLabelNumber
                      //   ? FONT_REGULAR
                      //   : FONT_BOLD,
                    }}
                  />
                );
              })}
            </View>
          );
        })}
        {!loading && (
          <Text style={[{textAlign: 'center', padding: appDimensions.NORMAL}]}>
            {'Screen Side'.toUpperCase()}
          </Text>
        )}
      </View>
    </ZoomHandler>
  );
});

export const SeatLayout = memo(SeatLayoutC);
