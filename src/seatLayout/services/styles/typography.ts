import {scaleFont} from './mixins';
import {Colors} from './index';

// FONT WEIGHT
export const FONT_WEIGHT_REGULAR = '400';
export const FONT_WEIGHT_BOLD = '700';

//FOR TEXT SIZE
export const textSize = {
  HEADING1: scaleFont(24),
  HEADING2: scaleFont(22),
  HEADING3: scaleFont(20),
  HEADING4: scaleFont(18),

  LARGE: scaleFont(17),
  NORMAL: scaleFont(16),
  SMALL: scaleFont(15),
  XSMALL: scaleFont(14),
  XXSMALL: scaleFont(12),
  XXXSMALL: scaleFont(10),
  SMALLEST: scaleFont(8),
};

// FONT STYLE

const genericBold = {
  fontWeight: FONT_WEIGHT_BOLD,
  color: Colors.PRIMARY_TEXT,
};

const genericNormal = {
  fontWeight: FONT_WEIGHT_REGULAR,
  color: Colors.PRIMARY_TEXT,
};
const genericHeader = {
  fontWeight: FONT_WEIGHT_REGULAR,
  color: Colors.PRIMARY_TEXT,
};

export const FONT_BOLD = {
  ...genericBold,
  fontSize: textSize.NORMAL,
};
export const FONT_BOLD_HEADER3 = {
  ...genericBold,
  fontSize: textSize.HEADING3,
};
export const FONT_BOLD_XXSMALL = {
  ...genericBold,
  fontSize: textSize.XXSMALL,
};
export const FONT_BOLD_XSMALL = {
  ...genericBold,
  fontSize: textSize.XSMALL,
};
export const FONT_BOLD_SMALL = {
  ...genericBold,
  fontSize: textSize.SMALL,
};

export const FONT_HEADER = {
  ...genericHeader,
  fontSize: textSize.HEADING3,
};
export const FONT_HEADER4 = {
  ...genericHeader,
  fontSize: textSize.HEADING4,
};
export const FONT_HEADER1 = {
  ...genericHeader,
  fontSize: textSize.HEADING1,
};
export const FONT_HEADER2 = {
  ...genericHeader,
  fontSize: textSize.HEADING2,
};

export const FONT_LARGE = {
  ...genericNormal,
  fontSize: textSize.LARGE,
};

export const FONT_REGULAR = {
  ...genericNormal,
  fontSize: textSize.NORMAL,
};

export const FONT_SMALL = {
  ...genericNormal,
  fontSize: textSize.SMALL,
};

export const FONT_XSMALL = {
  ...genericNormal,
  fontSize: textSize.XSMALL,
};

export const FONT_XXSMALL = {
  ...genericNormal,
  fontSize: textSize.XXSMALL,
};
