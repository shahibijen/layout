import React,{useContext} from 'react';
import axios from 'axios';
import {useDispatch } from 'react-redux';
import {Login_Successful,Login_failed,Login_Start,
        SingUpStart,SingUpSuccess,SingUpFailed,
        LogoutAction ,GetProfileStart,GetProfileSuccess,GetProfileFailed,
        ResetPasswordStart,ResetPasswordSuccess,ResetPasswordFailed
        } from './action/action';
import { useSelector } from 'react-redux';
export interface AuthContextInterface {
    SignUpFunc:(name : string,email : string,password:string)=> void,
    LoginFunc:(email : string,password:string)=> void ,
    LogoutFunc:()=> void ,
    ResetPasswordFunc:(id: number, name : string, email : string, location : string,Token : string)=> void ,
}

const AuthContext = React.createContext<AuthContextInterface | undefined>(undefined);


const AuthContainer: React.FC = ({children}) => { 
    const dispatch = useDispatch();

    const SignUpFunc =  (name : string,email : string,password:string) => 
    {    
        dispatch(SingUpStart());
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const body = JSON.stringify({ name,email, password });
        try {
            axios.post(`http://restapi.adequateshop.com/api/authaccount/registration`, body, config).then( function(response){
                console.log("sign up funnnc works",response)
                dispatch(SingUpSuccess());
                LoginFunc(email , password);
            }).catch(function (error) {
                dispatch(SingUpFailed());
                console.log(error);
            })
        }catch (err) {      
            dispatch(SingUpFailed());  
            console.log(err)   
        }
    }
    const LoginFunc =  (Token : string) => 
    {
        dispatch(Login_Start());
        dispatch(Login_Successful(Token));        
    }
    const GetProfile = (id:number , Token : number) => {
        dispatch(GetProfileStart())
        const config = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : `Bearer ${Token}`
            }
        };
        try {
             axios.get(`http://restapi.adequateshop.com/api/users/${id}`, config).then( function(response){
                dispatch(GetProfileSuccess(response.data))
            }).catch(function (error) {
                // handle error
                dispatch(GetProfileFailed())
                console.log(error);
            })
        } catch (err) {
            dispatch(GetProfileFailed())
            console.log(err)
        }

    }

    const LogoutFunc = () =>{
        dispatch(LogoutAction())
    }


    const ResetPasswordFunc =  (id: number, name : string, email : string, location : string, Token : string) => 
    {
        dispatch(ResetPasswordStart())
        const config = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : `Bearer ${Token}`
            }
        };
        const body = JSON.stringify({ id,name,email,location });
        try {
             axios.put(`http://restapi.adequateshop.com/api/users/${id}`,body,config).then( function(response){
                console.log('is this profile',response.data)
                dispatch(ResetPasswordSuccess())
            }).catch(function (error) {
                // handle error
                dispatch(ResetPasswordFailed())
                console.log(error);
            })
        } catch (err) {
            dispatch(ResetPasswordFailed())
            console.log(err)
        }

    }

  return (
    <AuthContext.Provider value={{SignUpFunc,LoginFunc,LogoutFunc,ResetPasswordFunc}} > 
        {children} 
    </AuthContext.Provider> 
  )
}


const withAuth = (OrignalComponent : React.FC | any) =>({...props}) => (
    <AuthContainer>
        <OrignalComponent {...props} />
    </AuthContainer>
)

export{ AuthContext,withAuth}