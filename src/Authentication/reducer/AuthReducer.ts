import {LOG_IN_START,LOG_IN_SUCCESS,LOGOUT,LOG_IN_FAILED,
        SING_UP_SUCCESS,SING_UP_FAILED,SING_UP_START,
        GET_USER_START,GET_USER_SUCCESS,GET_USER_FAILED,
        RESET_PASSWORD_START,RESET_PASSWORD_SUCCESS,RESET_PASSWORD_FAILED
      }from '../Types/AuthTypes';;

import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistReducer  } from "redux-persist";
      
const persistConfig = {
  key: 'user',
  storage: AsyncStorage,
};

export interface LoginDataState{
  Token : String,
  Loggedin : Boolean, 
  isloading : Boolean,
  Name : string,
  email : string,
  Country : string
}

const intialState = {
    Token : '',
    Loggedin : false,
    isloading : false,
    Name : '',
    email : '',
    Country : ''
}
export interface Action{
    type : String,
    data : any
  }
const LoggedInReducer = (state : LoginDataState = intialState,action:Action) =>{
    switch(action.type){
        case LOG_IN_START: {
          return{
            ...state , isloading : true
          }
        }
        case LOG_IN_SUCCESS: {
          console.log('reducer', action.data)
          return{
            ...state , Token : action.data , Loggedin : true, isloading : false
          }
        }
        case LOG_IN_FAILED: {
          return{
            ...state , isloading : false
          }
        }
        case LOGOUT :{
          return {
            ...state, Token : '', Loggedin : false, Name : '', email : '', Country : ''
          }
        }
        case SING_UP_START:
        case GET_USER_START:
        case RESET_PASSWORD_START:{
          return {
            ...state, isloading : true
          }
        }
        case SING_UP_SUCCESS:
        case RESET_PASSWORD_SUCCESS:{
          return {
            ...state, isloading : false
          }
        }
        case SING_UP_FAILED :
        case GET_USER_FAILED:
        case RESET_PASSWORD_FAILED:{
          return {
            ...state, isloading : false
          }
        }
        case GET_USER_SUCCESS: {
          return{
           ...state, Name : action.data.name , email : action.data.email, Country : action.data.location,isloading : false
          }
        }
        default: {
          return state;
        }
    }
}
export default persistReducer(persistConfig, LoggedInReducer);