export const LOG_IN_START = 'login_Start'
export const LOG_IN_SUCCESS = 'login_Success'
export const LOG_IN_FAILED = 'login_Failed'

export const GET_USER_START = 'get_user_start'
export const GET_USER_SUCCESS = 'get_user_success'
export const GET_USER_FAILED = 'get_user_failed'

export const LOGOUT = 'logout'

export const SING_UP_START = 'signup_Start'
export const SING_UP_SUCCESS = 'signup_Success'
export const SING_UP_FAILED = 'signup_Failed'

export const RESET_PASSWORD_START = 'Reset_password_start'
export const RESET_PASSWORD_SUCCESS = 'Reset_password_success'
export const RESET_PASSWORD_FAILED = 'Reset_password_failed'